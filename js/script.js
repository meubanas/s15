

// let myFavorite = "Lechon";
// console.log(myFavorite);

// num25 = 150 + 9;
// console.log(num2);

// num3 = 100 * 90;
// console.log(num3);

// let isEngineer = true;
// console.log('Does Mark is engieer?' +isEngineer);

// let array = ['Mang inasal', 'Jolibee', 'Chowking', 'Samgyup', 'KFC']
// console.log(array);

// let favoriteSinger = {
// 	firstName: 'Aristotle',
// 	lastName: 'Pollisco',
// 	stageName: 'Gloc-9',
// 	birthDay: 'October 18, 1977',
// 	age:   43,
// 	bestAlbum: 'Magdalena',
// 	bestSong: 'Simpleng tao',
// 	isAcitve: 'Yes'

// }
// console.log(favoriteSinger);



// function division(num1, num25) {
// 	console.log(num1/num25);
// 	return	num1/num25;
// };
// 	let quotient = division(50, 5);
// 	console.log(`The result of the division is: ${quotient}`);

// // Mathematical operation
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;	

// // num1 = num1 + num4;
// // num1 += num4;
// // console.log(num1);


// num2 += num4
// console.log(num2);

// let string1 = "Boston";
// let string2 = "Celtics";

// string1 += string2;
// console.log(string1);

// num1  = num1 - string1;
// console.log(num1);

// // Mathematical operations - follows MDAS

// // let mdasResult = 1 + 2  3 * 4 / 5;
// // console.log(mdasResult);

// // // PENDAS - Parenthesis, exponents, multiplication, division, addition and subtraction

// // let pendasResult = 1 + (2-3) * (4/5);
// // console.log(pendasResult);

// // Increment and Decrement
// //  two types increment: pre-fix and  post-fix

// let z = 1;
// // Pre-fix Incrementation
// ++=;
// console.log(z);

// // Post-fix incrementation
// z++;
// console.log(z);

let n = 1;
console.log(++n);
console.log(n);

console.log(n++);
console.log(n);


// comparison operators - used to compare values
// equality or loose  equality operator (==)

console.log(1 == 1); //true
console.log('1'== 1); //true

// strict equality
console.log(1 === 1);
console.log('1' === 1);

console.log('apple'== 'Apple');
let isSame = 55 == 55;
console.log(isSame);

console.log(0 == false); //force coercion
console.log (1 == true);
console.log(true == 'true');

console.log(true == '1');

// strict equality - checks both value and type
console.log (1 ==='1');

let name1 = 'Juan';

let x = 500;
let u = 700;
let w = 8000;

let requiredLevel = 45;
let requiredAge = 18;

let userName1 = 'gamer2021';
let userName2 = 'shadowMaster';
let userAge1 = 15;
let userAge2 = 30;

let registration1 = userName1.length > 9 && userAge1 >= requiredAge;
console.log(registration1); //false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

// OR  Operator (// - double pipe)
/*
	- or operator returns true if at least one of the operands are true

	T // T = T
	T // F = T
	F // T = T
	F // F = F
*/

// Not operator (!)
// // it turns a boolean into the opposite value: T = F vv F = T

// let opposite1 = !isAdmin;
// let opposite2 = !isLegalAge;

// console.log(opposite1);
// console.log(opposite2);

// if - if statement will run a code block if the condition specified is
// true or results to true.
const candy = 100;
if (candy >= 100){
	console.log('You got a cavity!');
}

/*
		if(true){
			block of  code
		};
*/

let userName3 = "Crusade_1993";
let userLevel3 = 25;
let userAge3 = 30;

if(userName3.length > 10){
	console.log("Welcome to the Game online!");
}

// else statement will be run if the condition given is false of results
// to  false.

// if(userName4 >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
// 	console.log("Thank you for joining Noobies Guild");
// } else {
// 	console.log ("You too strong to be noob. :( ");
// }

// else if - executes a statement, if the previous or the original condition
// is false or resulted to false but another specified condition resulted
// to true.

if(userName3.length >= 10 && userLevel3 <+ 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild.");
} else if(userLevel3 > 25){
	console.log("You too strong to be noob.");
} else if(userAge3 < requiredAge){
	console.log("You're too young to join the guild.");
} else {
	console.log("End of the condition");
}

// if-else in function
function addNum(num1, num2){
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types.");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers.");
	}
}

addNum(5, 2);


// mini-activity
function login(username, password){
	if(typeof username === "string" && typeof password === "string"){
		console.log("both arguments are string.");
		
		const username = loginForm.username.value;
		const password = loginForm.password.value;

		}else if(username === "user" && password === "websea") {
			alert("You have successfully logged in.");


		}else if (username.length > 8){
			console.log("#");

		}else if (password.length > 8){
			alert("#");

		}else if (username.length < 8){
			alert("minimum 8 letter");

		}else if (password.length < 8){
			alert("minimum 8 letter");

		} else {
			console.log("End of the condition")
		}
	}
